from flask import Flask,render_template,request,redirect,url_for
from flask_sqlalchemy import SQLAlchemy
import pygal

app = Flask(__name__)
#uniform resource indetifier
#each database table = each model.py
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:taylor17@127.0.0.1:5433/task_manager'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = 'taylor177'
db = SQLAlchemy(app)

from models import Tasks_model



@app.before_first_request

def create():
    db.create_all()



@app.route('/')
def hello_world():
    tasks = Tasks_model.read_all()
    # read on list comprehension
    status_list = [x.status for x in tasks]
    print(status_list)
    Pie_chart = pygal.Pie()
    Pie_chart.title = 'Task status'
    Pie_chart.add('completed projects',status_list.count('Complete'))
    Pie_chart.add('pending projects',status_list.count('Pending'))
    Pie_chart.add('cancelled projects',status_list.count('Cancelled'))
    graph = Pie_chart.render_data_uri()
    # print(graph)

    return render_template('index.html',tasks = tasks,graph = graph)

@app.route('/new',methods = ['POST'])
def new_task():
    if request.method =='POST':
        title = request.form['title']
        description = request.form['description']
        start_date = request.form['start_date']
        end_date = request.form['end_date']
        status = request.form['status']

        task = Tasks_model(title=title,description=description,start_date=start_date,end_date=end_date,status=status)

        task.insert_record()

        return redirect(url_for('hello_world'))
    else:
        return redirect(url_for('hello_world'))


@app.route('/edit/<int:id>',methods = ['POST'])
def edit_task(id):
    if request.method =='POST':
        title = request.form['title']
        description = request.form['description']
        start_date = request.form['start_date']
        end_date = request.form['end_date']
        status = request.form['status']
        # task = Tasks_model(title=title,description=description,start_date=start_date,end_date=end_date,status=status)
        Tasks_model.update_by_id(id=id,title=title,description=description,start_date=start_date,end_date=end_date,status=status)
        # task.insert_record()

        return redirect(url_for('hello_world'))
    else:
        return redirect(url_for('hello_world'))

@app.route('/delete/<int:id>')
def delete_task(id):
    if request.method == 'GET':
        Tasks_model.delete_by_id(id)
        return redirect(url_for('hello_world'))



if __name__ == '__main__':
    app.run()
