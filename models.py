from app import db
class Tasks_model(db.Model):
    __tablename__ = 'task'
    id  = db.Column(db.INTEGER,primary_key=True)
    title = db.Column(db.String(50),nullable=False)
    description = db.Column(db.String(500))
    start_date = db.Column(db.String(100))
    end_date = db.Column(db.String(100))
    status = db.Column(db.String(50))
    #create a new record
    def insert_record(self):
        db.session.add(self)
        db.session.commit()

    #read all records
    @classmethod
    def read_all(cls):
        return cls.query.all()
    # update a record
    @classmethod
    def update_by_id(cls,id,title = None,description = None,start_date = None,end_date = None,status = None):
        record = cls.query.filter_by(id = id).first()
        if title:
            record.title = title
        if description:
            record.description = description
        if start_date:
            record.start_date = start_date
        if end_date:
            record.end_date = end_date
        if status:
            record.status = status
        db.session.commit()
        return True

    # deletting records
    @classmethod
    def delete_by_id(cls,id):
        record = cls.query.filter_by(id = id)

        if record.first():
            record.delete()
            db.session.commit()
            return True
        else:
            return False